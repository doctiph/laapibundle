<?php
namespace La\ApiBundle\Exception;

use FOS\RestBundle\Util\Codes;

/**
 * Class InvalidClientCredentialsException
 * @package La\ApiBundle\Exception
 */
class InvalidClientCredentialsException extends \Exception implements ApiExceptionInterface
{

    public function getHttpErrorCode()
    {
        return Codes::HTTP_UNAUTHORIZED;
    }

    public function getApiMessage()
    {
        return 'invalid_client_credentials';
    }

    public function getHttpHeaders()
    {
        return array('WWW-Authenticate' => 'Basic realm="Secured Api"');
    }

}