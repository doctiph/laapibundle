<?php
namespace La\ApiBundle\Exception;

use FOS\RestBundle\Util\Codes;

/**
 * Class InvalidEncodedParameterException
 * @package La\ApiBundle\Exception
 */
class InvalidEncodedParameterException extends \Exception implements ApiExceptionInterface
{

    public function getHttpErrorCode()
    {
        return Codes::HTTP_BAD_REQUEST;
    }

    public function getApiMessage()
    {
        return 'invalid_credentials_encoding';
    }

}