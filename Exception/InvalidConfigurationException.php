<?php
namespace La\ApiBundle\Exception;

use FOS\RestBundle\Util\Codes;

/**
 * Class InvalidConfigurationException
 * @package La\ApiBundle\Exception
 */
class InvalidConfigurationException extends \Exception implements ApiExceptionInterface
{

    public function getHttpErrorCode()
    {
        return Codes::HTTP_BAD_REQUEST;
    }

    public function getApiMessage()
    {
        return 'invalid_configuration';
    }

}