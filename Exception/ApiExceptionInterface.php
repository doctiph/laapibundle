<?php
namespace La\ApiBundle\Exception;

/**
 * Class ApiExceptionInterface
 * @package La\ApiBundle\Exception
 */
interface ApiExceptionInterface
{
    public function getHttpErrorCode();
    public function getApiMessage();
}