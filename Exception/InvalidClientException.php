<?php
namespace La\ApiBundle\Exception;

use FOS\RestBundle\Util\Codes;

/**
 * Class InvalidClientException
 * @package La\ApiBundle\Exception
 */
class InvalidClientException extends \Exception implements ApiExceptionInterface
{

    public function getHttpErrorCode()
    {
        return Codes::HTTP_UNAUTHORIZED;
    }

    public function getApiMessage()
    {
        return 'invalid_client';
    }

    public function getHttpHeaders()
    {
        return array('WWW-Authenticate' => 'Basic realm="Secured Api"');
    }

}