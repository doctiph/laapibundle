<?php

namespace La\ApiBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html#cookbook-bundles-extension-config-class}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritDoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('la_api');

        $rootNode->
            children()
                ->arrayNode('clients')
                ->performNoDeepMerging()    // @todo Trouver meilleure solution pour surcharger cette config en test ou dev
                ->prototype('array')
                    ->children()
                        ->scalarNode('password')
                            ->isRequired()
                        ->end()
                        ->scalarNode('role')
                            ->defaultValue('ROLE_API_CLIENT')
                        ->end()
                    ->end()
                ->end()
            ->end()
        ;

        return $treeBuilder;
    }
}
