<?php

namespace La\ApiBundle\Model;

use La\ApiBundle\Event\ApiDocEvent;
use Symfony\Component\EventDispatcher\ContainerAwareEventDispatcher;

class ApiDoc
{
    const API_COMMON_NAME = 'api';

    protected $eventDispatcher;

    public function __construct(ContainerAwareEventDispatcher $eventDispatcher)
    {
        $this->eventDispatcher = $eventDispatcher;
    }

    public function getApiCommenName()
    {
        return static::API_COMMON_NAME;
    }

    public function getJsonFiles($name)
    {
        $files = [];
        $dirs = $this->getDocRoot();

        foreach ($dirs as $dir) {
            $dir .= DIRECTORY_SEPARATOR . $this->getApiCommenName();
            if (is_dir($dir)) {
                if ($dh = opendir($dir)) {
                    while (($file = readdir($dh)) !== false) {
                        if ($file === '.' || $file === '..' || is_dir($dir . '/' . $file)) {
                            continue;
                        }
                        $filename = pathinfo($file)['filename'];
                        $filename = str_replace('$', ':', $filename);
                        $filename = str_replace('.', '/', $filename);
                        $filename = str_replace('_' . $this->getApiCommenName() . '_', '_' . $name . '_', $filename);

                        if (file_exists($dir . DIRECTORY_SEPARATOR . $file)) {
                            $files [$filename] = $dir . DIRECTORY_SEPARATOR . $file;
                        }
                    }
                    closedir($dh);
                }
            }
        }

        foreach ($dirs as $dir) {
            $dir .= DIRECTORY_SEPARATOR . $name;
            if (is_dir($dir)) {
                if ($dh = opendir($dir)) {
                    while (($file = readdir($dh)) !== false) {
                        if ($file === '.' || $file === '..' || is_dir($dir . '/' . $file)) {
                            continue;
                        }
                        $filename = pathinfo($file)['filename'];
                        $filename = str_replace('$', ':', $filename);
                        $filename = str_replace('.', '/', $filename);

                        if (file_exists($dir . DIRECTORY_SEPARATOR . $file)) {
                            $files [$filename] = $dir . DIRECTORY_SEPARATOR . $file;
                        }
                    }
                    closedir($dh);
                }
            }
        }

        return $files;
    }

    public function getDocRoot()
    {
        $event = new ApiDocEvent();
        $this->eventDispatcher->dispatch('api.doc.root', $event);

        return $event->getDocRootDirs();
    }


} 