ApiBundle
============

# Installation

### Pour l'exemple nous utiliserons le site elle.fr

## Update composer.json :

    "repositories": [
#....
        {
            "type": "git",
            "url": "git@git01.in.ladtech.fr:LaApiBundle"
        },
    ],
#....
    "require": {
        "jms/serializer-bundle": "0.13.*@dev",
        "friendsofsymfony/rest-bundle": "1.0.*@dev",
        "la/apibundle": "dev-master"
    },

## Update /app/AppKernel.php

     public function registerBundles()
     {
         $bundles = array(
             // ....
             new FOS\RestBundle\FOSRestBundle(),
             new JMS\SerializerBundle\JMSSerializerBundle($this),
             new La\ApiBundle\LaApiBundle(),
             // ....
         );
     }

# Import des fichiers de configurations communs

### app/config/routing.yml

    la_api:
        resource: "@LaApiBundle/Resources/config/routing.yml"
        prefix:   /

# Configuration

### app/config/config.yml

    fos_rest:
        view:
            view_response_listener: force
        routing_loader:
            default_format: json

    sensio_framework_extra:
        view:    { annotations: false }
        router:  { annotations: true }

### app/config/la_config.yml

    la_api:
        clients:                    # Définition des clients autorisés à utiliser l'API.
            web:
                password: 123456


### app/config/security.yml

Définir la route /api dans security.yml

    security:
        encoders:
# ....
            La\ApiBundle\Entity\Client: plaintext
        providers:
            api_client:
                id: security.user.provider.api_client
# ....
        firewalls:
            apidoc:
                pattern: ^/api/doc/
                http_basic:
                    realm: "Secured Api Area"
                    provider: api_client
            api:
                pattern: ^/api/
                http_basic:
                    realm: "Secured Api Area"
                    provider: api_client
                stateless:  true
# ....
        access_control:
# ....
            - { path: ^/api$, role: ROLE_API_CLIENT }

