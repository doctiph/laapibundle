<?php

namespace La\ApiBundle\Controller;

use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpKernel\Exception\HttpException;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Util\Codes;
use La\ApiBundle\Exception as ApiException;

abstract class AbstractApiController extends FOSRestController
{
    const KEY = 'error';

    public function errorAction()
    {
        return $this->send(
            array(
                'error_message' => 'invalid_query',
            ), Codes::HTTP_NOT_FOUND);
    }

    protected function send($data, $code, $headers = array())
    {
        return $this->view(array(static::KEY => $data), $code, $headers);
    }

    protected function sendError($error)
    {
        // Thrown exceptions : known errors
        if ($error instanceof ApiException\ApiExceptionInterface) {

            $content = array(
                'error_key' => $error->getApiMessage()
            );
            $statusCode = $error->getHttpErrorCode();
            $headers = method_exists($error, 'getHttpHeaders') ? $error->getHttpHeaders() : array();

        }
        // Form error : validation failed
        else if (is_array($error) && isset($error['form'])) {

            $content = array(
                    'error_key' => 'validation_failed',
                    'form' => $error['form']
            );
            $statusCode = Codes::HTTP_BAD_REQUEST;
            $headers = array();

        }
        // Unknown errors
        else {

            $content = array(
                'error_key' => $error instanceof \Exception ? $error->getMessage() : $error
            );
            $statusCode = $error instanceof HttpException ? $error->getStatusCode(): Codes::HTTP_INTERNAL_SERVER_ERROR;
            $headers = array();

        }

        return $this->send($content, $statusCode, $headers);
    }

    protected function getErrors($form, $request)
    {
        /** @var FormInterface $form */
        $form->submit($request);

        if (!$form->isValid()) {

            $errors = array();
            $errorList = $this->get('validator')->validate($form);

            foreach ($errorList as $error) {
                $propertyPath = $error->getPropertyPath();
                $message = array(
                    "error_key" => $this->container->get('translator')->trans($error->getMessageTemplate(), $error->getMessageParameters(), 'formapi'),
                    "error_message" => $this->container->get('translator')->trans($error->getMessageTemplate(), $error->getMessageParameters(), 'validators')
                );

                $explodedPath = explode('.', $propertyPath);

                foreach ($explodedPath as $i => $path) {
                    if ($path == 'data') {
                        unset($explodedPath[$i]);
                    }
                }

                $final = $this->getChildren($explodedPath, $message);
                $errors = array_merge($errors, $final);

                $tmp = array();
                $final = array();
                foreach ($explodedPath as $key) {

                    if (empty($final)) {
                        $final = $message;
                    } else {
                        $final[$key] = $tmp;
                    }
                }
            }
            return $errors;
        } else {
            return false;
        }
    }

    protected function getChildren($array, $message)
    {
        if (count($array)) {
            $key = array_shift($array);
            if (strpos($key, 'children[') !== false) {
                $key = str_replace('children[', '', $key);
                $key = str_replace(']', '', $key);
            }
            if (strpos($key, 'data[') !== false) {
                $key = str_replace('data[', '', $key);
                $key = str_replace(']', '', $key);
            }
            $result[$key] = $this->getChildren($array, $message);
        } else {
            return $message;
        }
        return $result;
    }


    protected function checkSecurity()
    {
        $logged = $this->get('security.context')->isGranted('ROLE_API_CLIENT');

        if (!$logged) {
            throw new ApiException\InvalidConfigurationException();
        }
        return true;
    }

    protected function getUniqueId($key = null)
    {
        $client = $this->get('security.context')->getToken()->getUsername();
        $uniqueId = md5(md5($this->container->getParameter('secret')) . md5($client));
        if (!is_null($key)) {
            $uniqueId = sprintf('%s_%s', $uniqueId, $key);
        }
        return $uniqueId;
    }

}