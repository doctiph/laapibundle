<?php

namespace La\ApiBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DocController extends Controller
{
    const API_RESOURCE_SEPARATOR = ' ';
    const API_COMMON_NAME = 'api';

    protected function send(Response $response)
    {
        $response->setMaxAge(7200);
        return $response;
    }

    public function homeAction(Request $request, $name)
    {
        $links = $this->getLinks($name);
        $resource = $this->getResourceName($name);

        $response = $this->get('templating')->renderResponse('LaApiBundle:Doc:home.html.twig', array(
            'links' => $links, 'name' => $name, 'resource' => $resource
        ));
        return $this->send($response);
    }

    public function homeAppendixAction(Request $request, $name)
    {
        $links = $this->getLinksAppendix($name);
        $resource = $this->getResourceName($name);

        $response = $this->get('templating')->renderResponse('LaApiBundle:Doc:home_appendix.html.twig', array(
            'links' => $links, 'name' => $name, 'resource' => $resource
        ));
        return $this->send($response);
    }

    public function appendixAction(Request $request, $name, $action)
    {
        $current = $this->getCurrentAppendixName($name, $action);
        $resource = $this->getResourceName($name, $action);
        $data = $this->loadJsonAppendix($name, $action, $resource, $current);
        if (!isset($data[$resource])) {
            throw new \Exception(sprintf('No documentation for appendix %s/%s', $name, $action));
        } else {
            $infos = $data[$resource];
        }
        if (isset($infos['body'])) {
            foreach ($infos['body'] as $key => $value) {
                if (isset($value['example'])) {
                    $infos['body'][$key]['example'] = json_encode($value['example'], JSON_PRETTY_PRINT);
                }
            }
        }
        if (isset($infos['example'])) {
            $example = json_encode($data[$resource]['example'], JSON_PRETTY_PRINT);
            $infos['example'] = $example;
        }

        $response = $this->get('templating')->renderResponse('LaApiBundle:Doc:appendix.html.twig', array(
            'name' => $name, 'infos' => $infos, 'current' => $current, 'action' => $action
        ));
        return $this->send($response);
    }

    public function resourceAction(Request $request, $method, $name, $action = null)
    {
        $current = $this->getCurrentName($method, $name, $action);
        $resource = $this->getResourceName($name, $action);
        $data = $this->loadJsonData($method, $name, $action);
        $method = strtoupper($method);

        if (!isset($data[$resource][$method])) {
            throw new \Exception(sprintf('No documentation for %s %s', $method, $resource));
        } else {
            $infos = $data[$resource][$method];
        }

        if (isset($infos['example'])) {
            if (isset($infos['example']['response'])) {
                $example = json_encode($data[$resource][$method]['example']['response'], JSON_PRETTY_PRINT);
                $infos['example']['response'] = $example;
            }
        }

        $response = $this->get('templating')->renderResponse('LaApiBundle:Doc:resource.html.twig', array(
            'name' => $name, 'infos' => $infos, 'method' => $method, 'resource' => $resource, 'current' => $current
        ));
        return $this->send($response);
    }


    public function listAction($name, $current = null)
    {
        $links = $this->getLinks($name);

        $response = $this->get('templating')->renderResponse('LaApiBundle:Doc:list.html.twig', array(
            'links' => $links, 'current' => $current
        ));
        return $this->send($response);
    }

    public function listAppendixAction($name, $current = null)
    {
        $links = $this->getLinksAppendix($name);

        $response = $this->get('templating')->renderResponse('LaApiBundle:Doc:list_appendix.html.twig', array(
            'links' => $links, 'current' => $current
        ));
        return $this->send($response);
    }

    protected function loadJsonData($method, $name, $action = null)
    {
        $dirs = $this->get('la_api.api.doc.model')->getDocRoot();
        if (!is_null($action)) {
            $action = str_replace('/', '.', $action);
            $action = str_replace(':', '$', $action);
        }

        foreach ($dirs as $dir) {
            if (is_null($action)) {
                $file = sprintf('%s/%s/%s_%s.json', $dir, $name, $method, $name);
                if (file_exists($file)) {
                    $data = json_decode(file_get_contents($file), true);
                    return $data;
                }
            } else {
                $file = sprintf('%s/%s/%s_%s_%s.json', $dir, $name, $method, $name, $action);
                if (file_exists($file)) {

                    $data = json_decode(file_get_contents($file), true);
                    return $data;
                }
            }
        }

        if (is_null($action)) {
            throw new \Exception (sprintf('Undefined doc api file for %s.json', $name));
        } else {
            throw new \Exception (sprintf('Undefined doc api file for %s/%s.json', $name, $action));
        }
    }

    protected function loadJsonAppendix($name, $action, &$resource, &$current)
    {
        $dirs = $this->get('la_api.api.doc.model')->getDocRoot();

        foreach ($dirs as $dir) {
            $file = sprintf('%s/%s/appendix_%s_%s.json', $dir, $name, $name, $action);
            $commonFile = sprintf('%s/%s/appendix_%s_%s.json', $dir, $this->get('la_api.api.doc.model')->getApiCommenName(), $this->get('la_api.api.doc.model')->getApiCommenName(), $action);
            if (file_exists($file)) {
                $data = json_decode(file_get_contents($file), true);
                return $data;
            } elseif (file_exists($commonFile)) {
                $resource = $this->getResourceName($this->get('la_api.api.doc.model')->getApiCommenName(), $action);
                $current = $this->getCurrentAppendixName($this->get('la_api.api.doc.model')->getApiCommenName(), $action);

                $data = json_decode(file_get_contents($commonFile), true);
                return $data;
            }
        }
        throw new \Exception (sprintf('Undefined doc api file for appendix/%s/%s.json', $name, $action));
    }

    protected function getResourceName($name, $action = null)
    {
        if (is_null($action)) {
            $resource = $name;
        } else {
            $resource = sprintf('%s/%s', $name, $action);
        }
        return $resource;
    }

    protected function getCurrentName($method, $name, $action = null)
    {
        $current = sprintf('%s%s%s%s%s', $name, static::API_RESOURCE_SEPARATOR, $action, static::API_RESOURCE_SEPARATOR, $method);
        return $current;
    }

    protected function getCurrentAppendixName($name, $action = null)
    {
        $current = sprintf('appendix_%s_%s', $name, $action);
        return $current;
    }

    protected function getLinks($name)
    {
        $links = array();
        $files = $this->get('la_api.api.doc.model')->getJsonFiles($name);
        foreach ($files as $filename => $file) {

            $ex = explode('_', $filename);
            if (count($ex) == 3) {
                $action = $ex[2];
            } else {
                $action = null;
            }
            $method = $ex[0];
            if (!in_array($method, array('post', 'put', 'get', 'option', 'patch', 'delete'))) {
                continue;
            }

            $key = $this->getCurrentName($method, $name, $action);
            if (array_key_exists($key, $links)) {
                continue;
            }
            $json = file_get_contents($file);
            $data = json_decode($json, true);

            $resource = $this->getResourceName($name, $action);
            $description = $data[$resource][strtoupper($method)]['short_description'];

            $dateChanges = $this->getDateChange($data[$resource][strtoupper($method)]['changelog']);

            $links[$key] = array_merge($dateChanges, array(

                'resource' => $resource,
                'method' => $method,
                'name' => $name,
                'action' => $action,
                'description' => $description,
            ));
        }
        ksort($links);
        return $links;
    }

    protected function getLinksAppendix($name)
    {

        $links = array();
        $files = $this->get('la_api.api.doc.model')->getJsonFiles($name);
        foreach ($files as $filename => $file) {
            $filename = pathinfo($file)['filename'];
            if (array_key_exists($filename, $links)) {
                continue;
            }

            $ex = explode('_', $filename);
            if (!in_array($ex[0], array('appendix'))) {
                continue;
            }
            if ($name !== $ex[1] && $this->get('la_api.api.doc.model')->getApiCommenName() !== $ex[1]) {
                continue;
            }
            $action = $ex[2];
            $realName = $ex[1];

            $json = file_get_contents($file);
            $data = json_decode($json, true);
            $resource = $this->getResourceName($realName, $action);
            $description = $data[$resource]['short_description'];
            $title = $data[$resource]['title'];

            $dateChanges = $this->getDateChange($data[$resource]['changelog']);

            $links[$filename] = array_merge($dateChanges, array(
                'resource' => $resource,
                'name' => $name,
                'action' => $action,
                'description' => $description,
                'title' => $title,
                'key_for_sorting' => $filename,
            ));
        }
        $tmp = [];
        foreach ($links as $k => $link) {
            $tmp[$link['title'] . '_' . md5($k)] = $link;
        }
        ksort($tmp);
        $links = [];
        foreach ($tmp as $k => $link) {
            $links[$link['key_for_sorting']] = $link;
        }
        return $links;
    }

//    protected function getDocRoot()
//    {
//        $dispatcher = $this->container->get('event_dispatcher');
//        $event = new ApiDocEvent();
//        $dispatcher->dispatch('api.doc.root', $event);
//
//        return $event->getDocRootDirs();
//    }
//

//    protected function getJsonFiles($name, $common = false)
//    {
//        $files = [];
//        $dirs = $this->get('la_api.api.doc.model')->getDocRoot();
//
//        if (false !== $common) {
//            foreach ($dirs as $dir) {
//                $dir .= DIRECTORY_SEPARATOR . $common;
//                if (is_dir($dir)) {
//                    if ($dh = opendir($dir)) {
//                        while (($file = readdir($dh)) !== false) {
//                            if ($file === '.' || $file === '..' || is_dir($dir . '/' . $file)) {
//                                continue;
//                            }
//                            $filename = pathinfo($file)['filename'];
//                            $filename = str_replace('$', ':', $filename);
//                            $filename = str_replace('|', '/', $filename);
//
//                            if (file_exists($dir . DIRECTORY_SEPARATOR . $file)) {
//                                $files [$filename] = $dir . DIRECTORY_SEPARATOR . $file;
//                            }
//                        }
//                        closedir($dh);
//                    }
//                }
//            }
//        }
//
//        foreach ($dirs as $dir) {
//            $dir .= DIRECTORY_SEPARATOR . $name;
//            if (is_dir($dir)) {
//                if ($dh = opendir($dir)) {
//                    while (($file = readdir($dh)) !== false) {
//                        if ($file === '.' || $file === '..' || is_dir($dir . '/' . $file)) {
//                            continue;
//                        }
//                        $filename = pathinfo($file)['filename'];
//                        $filename = str_replace('$', ':', $filename);
//                        $filename = str_replace('|', '/', $filename);
//
//                        if (file_exists($dir . DIRECTORY_SEPARATOR . $file)) {
//                            $files [$filename] = $dir . DIRECTORY_SEPARATOR . $file;
//                        }
//                    }
//                    closedir($dh);
//                }
//            }
//        }
//
//        return $files;
//    }

    protected function getDateChange($changelog)
    {
        $updated = array_keys($changelog)[0];

        $date = new \DateTime($updated);
        $now = new \DateTime();

        $interval = $date->diff($now);
        $modified = $interval->format('%a');

        if ($modified == '0') {
            $modifiedHours = $interval->format('%h');
        } else {
            $modifiedHours = 0;
        }

        if ($modified < 1) {
            $alertclass = "danger";
            $icon = "fire";

        } elseif ($modified < 7) {
            $alertclass = "warning";
            $icon = "warning-sign";

        } else {
            $alertclass = "info";
            $icon = "info-sign";
        }

        return array(
            'icon' => $icon,
            'modified_hours' => $modifiedHours,
            'modified' => $modified,
            'alertclass' => $alertclass,
            'updated' => $updated
        );
    }

}