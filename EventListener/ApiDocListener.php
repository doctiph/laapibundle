<?php

namespace La\ApiBundle\EventListener;


use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class ApiDocListener implements EventSubscriberInterface
{
    const API_DOC_ROOT_EVENT_PRIORITY = 1;

    public static function getSubscribedEvents()
    {
        return array(
            'api.doc.root' => array('onApiDocRootRequest', static::API_DOC_ROOT_EVENT_PRIORITY)
        );
    }

    public function onApiDocRootRequest($event)
    {
        $event->addDocRootDir(__DIR__ . '/../Resources/doc/api/resources');
    }

}