<?php

namespace La\ApiBundle\Twig;

use La\ApiBundle\Event\ApiDocEvent;
use La\ApiBundle\Model\ApiDoc;

class ApiExtension extends \Twig_Extension
{
    protected $dispatcher;
    protected $router;

    public function __construct($apiDocModel, $dispatcher, $router)
    {
        $this->apiDocModel = $apiDocModel;
        $this->dispatcher = $dispatcher;
        $this->router = $router;
    }

    public function getFilters()
    {
        return array(
            'urlizer' => new \Twig_Filter_Method($this, 'urlizerFilter', array('is_safe' => array('html'))),
        );
    }

    public function urlizerFilter($text, $context, $class = "")
    {
        $pattern = '/@([^@#]+)#?(.*)@/';
        $text = preg_replace_callback($pattern, function ($matches) use ($context, $class) {
            return $this->getFilename($matches, $context, $class);
        }, $text);
        return $text;
    }

    protected function getFilename($matches, $context, $class = '')
    {

        $prefix = $matches[1];
        $prefix = str_replace('$', ':', $prefix);
        $prefix = str_replace('.', '/', $prefix);
        $ex = explode('_', $prefix);
        $name = $ex[1];
        // on transform appendix_api_test en appendix_context_test

        if (isset($matches[1])) {
            $filename = sprintf('%s.json', $prefix);
        }
        $anchor = '';
        if (isset($matches[2]) && $matches[2] != '') {
            $anchor = $matches[2];
        }

        $dirs = $this->getDocRoot();

        $files = $this->apiDocModel->getJsonFiles($name);
        foreach ($files as $filename => $file) {
            if ($filename !== $prefix) {
                continue;
            }
            $json = file_get_contents($file);
            $data = json_decode($json, true);
            $data = array_values($data)[0];
            if (!isset($data['title'])) {
                // POST, GET, et.
                $data = array_values($data)[0];
            }

            if (isset($data['title'])) {

                $title = $data['title'];
                $description = $data['short_description'];
                if ('' !== $anchor) {
                    $title .= ' : ' . $anchor;
                    $anchor = '#' . $anchor;
                } else {
                    $hash = '';
                }

                if ('' !== $class) {
                    $class = "class=$class";
                }
                if ($name !== $context) {
                    $prefix = str_replace('_' . $name . '_', '_' . $context . '_', $prefix);
                }
                $uri = $this->getUri($prefix);
                return sprintf('<a %s href="%s%s" title="%s">%s</a>', $class, $uri, $anchor, $description, $title);
            }

        }
        foreach ($dirs as $dir) {
            if (file_exists($dir . DIRECTORY_SEPARATOR . $name . DIRECTORY_SEPARATOR . $filename)) {

                $json = file_get_contents($dir . DIRECTORY_SEPARATOR . $name . DIRECTORY_SEPARATOR . $filename);
                $data = json_decode($json, true);
                $data = array_values($data)[0];
                if (!isset($data['title'])) {
                    // POST, GET, et.
                    $data = array_values($data)[0];
                }

                if (isset($data['title'])) {

                    $title = $data['title'];
                    $description = $data['short_description'];
                    if ('' !== $anchor) {
                        $title .= ' : ' . $anchor;
                        $anchor = '#' . $anchor;
                    } else {
                        $hash = '';
                    }

                    if ('' !== $class) {
                        $class = "class=$class";
                    }

                    $uri = $this->getUri($prefix);
                    return sprintf('<a %s href="%s%s" title="%s">%s</a>', $class, $uri, $anchor, $description, $title);
                }

            }
        }
        return false;
    }

    protected function getUri($filename)
    {
        $ex = explode('_', $filename);
        if ($ex[0] === 'appendix') {
            if (count($ex) === 3) {
                $action = str_replace('$', ':', $ex[2]);
                $routename = 'la_api_doc_appendix';
                $uri = $this->router->generate($routename, array('name' => $ex[1], 'action' => $action));
            } else if (count($ex) === 2) {
                $routename = 'la_api_doc_resource';
                $uri = $this->router->generate($routename, array('name' => $ex[1]));
            }
        } else {
            if (count($ex) === 3) {
                $routename = 'la_api_doc_resource';
                $action = str_replace('$', ':', $ex[2]);
                $uri = $this->router->generate($routename, array('method' => $ex[0], 'name' => $ex[1], 'action' => $action));
            } else if (count($ex) === 2) {
                $routename = 'la_api_doc_resource';
                $uri = $this->router->generate($routename, array('method' => $ex[0], 'name' => $ex[1]));
            }
        }
        return $uri;
    }

    public function getName()
    {
        return 'api_extension';
    }

    protected function getDocRoot()
    {
        $event = new ApiDocEvent();
        $this->dispatcher->dispatch('api.doc.root', $event);
        return $event->getDocRootDirs();
    }
}
