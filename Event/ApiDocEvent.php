<?php

namespace La\ApiBundle\Event;

use Symfony\Component\EventDispatcher\Event;
use La\UserBundle\Entity\User;

class ApiDocEvent extends Event
{
    protected $docRootDirs;

    public function addDocRootDir($docRootDir)
    {
        $this->docRootDirs[] = $docRootDir;
    }

    public function getDocRootDirs()
    {
        return $this->docRootDirs;
    }

}